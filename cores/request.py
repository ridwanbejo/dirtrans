from cgi import escape
import sys, logging
import urlparse

class Request:
	def __init__(self):
		self.PARAM = []
		self.POST = {}
		self.GET = {}
		self.FILES = {}
		self.raw_request_body = ""
		self.raw_query_string = ""
		
	def create_request_object(self, environ):
		""" PROCESSING POST DATA """
		# mencatat raw_body_request
		# proses komposisi data POST
		try:
			request_body_size = int(environ.get('CONTENT_LENGTH', 0))
		except:
			print "Unexpected error request to server :", sys.exc_info()[0]
			logging.exception('')
			request_body_size = 0

		self.raw_request_body = environ['wsgi.input'].read(request_body_size).decode()
		self.POST = urlparse.parse_qs(self.raw_request_body,True)

		for item in self.POST:
			if len(self.POST.get(item, [])) == 1:
				self.POST[item] = self.POST.get(item, [''])[0]

		print self.POST
		
		""" PROCESSING GET DATA """
		# mencatat raw_query_string
		self.raw_query_string = environ['QUERY_STRING']
		# proses komposisi data GET
		self.GET = urlparse.parse_qs(environ['QUERY_STRING'], True)

		for item in self.GET:
			if len(self.GET.get(item, [])) == 1:
				self.GET[item] = self.GET.get(item, [''])[0]

		print self.GET

	def set_param(self, param):
		self.PARAM = param