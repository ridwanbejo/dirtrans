#DirTrans (Directory of Transportation System)

Simple web application for find information about transportation system in Indonesia

List of Special Feature:

- Find transportation endpoint by city
- Give detail information about transportation endpoint
- Give information about places near the transportation endpoint
- Transportation endpoint consists of shuttle bus, bus station, airport, port, and train station