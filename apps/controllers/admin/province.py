from cores.response import Response

class Province:
	"""
	Halaman umum dashboard seperti menu dan grafik
	"""
	def __init__(self, request):
		self.request = request

	def index(self):
		html = "<h1>Ini adalah halaman province</h1>"
		return Response().send(html)

	def add (self):
		return Response().render('admin/province_add.html')

	def add_process(self):
		html = self.request.POST['name'] + "<br/>"
		html = html + self.request.POST['description'] + "<br/>"
		return Response().send(html)