class User:
	"""
	CRUD user untuk halaman admin, dan user umum (beserta token)
	"""
	def index(self):
		resp_body = """<html>
						<head>
							<title>Admin Page - User</title>
						</head> 
						<body>
							<h1>Welcome to Admin Page - User!</h1>
						</body>
					</html>"""
					
		resp_status = '200 OK'
		resp_headers = [
							('Content-Type', 'text/html'),
							('Content-Length', str(len(resp_body)))
						 ]

		resp_data = {
			'body': resp_body,
			'status': resp_status,
			'headers': resp_headers
		}

		return resp_data