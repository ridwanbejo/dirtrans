class Transportation:
	"""
	CRUD informasi penyedia angkutan
	"""
	def index(self):
		resp_body = """<html>
						<head>
							<title>Admin Page - Transportation</title>
						</head> 
						<body>
							<h1>Welcome to Admin Page - Transportation!</h1>
						</body>
					</html>"""
					
		resp_status = '200 OK'
		resp_headers = [
							('Content-Type', 'text/html'),
							('Content-Length', str(len(resp_body)))
						 ]

		resp_data = {
			'body': resp_body,
			'status': resp_status,
			'headers': resp_headers
		}

		return resp_data