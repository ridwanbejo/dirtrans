from wsgiref.simple_server import make_server
from cgi import parse_qs, escape
from livereload import Server, shell
import os, sys, logging

"""
PROJECTS SETTING
"""

# set the project path
sys.path.append(os.getcwd())

from cores.request import Request
from cores.response import Response

# define URL Dictionary for URL Routing

"""
END PROJECTS SETTING
"""
def console_log(environment):
	print environment['PATH_INFO']
	
def show_environment(environment):
	# Sorting and stringifying the environment key, value pairs
	resp_body = ['%s: %s <br/>' % (key, value)
	                for key, value in sorted(environment.items())]
	resp_body = '\n'.join(resp_body)
	return Response().send(resp_body)

def show_error(error_type="404"):
	# Sorting and stringifying the environment key, value pairs
	if (error_type == "404"):
		resp_body = '<h2>Your requested page not found...</h@>'
	elif (error_type == "500"):
		resp_body = '<h2>500 internal server error...</h@>'
		
	return Response().send(resp_body)
	

def application(environment, start_response):
	console_log(environment)
	resp_data = show_error("500")
	
	# search the executor from url_dictionary
	# there are process for search executor from url_dictionary

	# otherwise  catch the incoming request path then map to the executor without url_dictionary (classes.method | modules.classes.method)
	# 0=folder, 1=class, 2=method, n=parameter
	incoming_request_path = environment['PATH_INFO']
	request_path_segment = incoming_request_path.split('/')

	request_path_index = 0
	modules = request_path_segment[request_path_index + 1]
	modules_dir = os.getcwd()+"/apps/controllers/"+modules
	
	if (os.path.isdir(modules_dir)):
		modules = request_path_segment[1]
		request_path_index = 1	
	else:
		modules = request_path_segment[0]
	
	classes = ""
	method = ""
	param = []

	try:
		classes = request_path_segment[request_path_index + 1]
		method = request_path_segment[request_path_index + 2]
		param = request_path_segment[(request_path_index + 3):]

	except:
		resp_data = show_error("404")

	# create an request information object GET, POST, PARAM
	request_obj = Request()
	request_obj.set_param(param)
	request_obj.create_request_object(environment)

	# mapping the incoming request path into the appropriate class and method
	import_str = "from apps.controllers."
	exec_str = ""
	if modules != "":
		import_str = import_str + modules + "."
		
	if classes != "":
		import_str = import_str + classes + " import "+ classes.capitalize()  
		exec_str = exec_str + classes.capitalize() +"(request_obj)."

	if method != "":
		exec_str = exec_str + method.lower() + "()"

	# bypassing the error of executor evaluation
	try:
		exec(import_str)
		resp_data = eval(exec_str)
	except:
		print "Unexpected error server:", sys.exc_info()[0]
		logging.exception('')
		resp_data = show_error("500")

	# display the response
	# resp_data = show_environment(environment)
	start_response(resp_data['status'], resp_data['headers'])
	return [resp_data['body']]

run_mode = "livereload"

if run_mode == "livereload": 
	"""
	LiveReload Server
	"""
	server = Server(application)
	server.serve(port=8084, host='localhost')
else:
	"""
	WSGI Server
	"""
	httpd = make_server('localhost', 8084, application)
	httpd.serve_forever()