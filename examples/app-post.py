from wsgiref.simple_server import make_server
from cgi import parse_qs, escape

html = """
<html>
<body>
   <form method="POST" action="/">
      <p>
         Age: <input type="text" name="age">
         </p>
      <p>
         Hobbies:
         <input name="hobbies" type="checkbox" value="software"> Software
         <input name="hobbies" type="checkbox" value="tunning"> Auto Tunning
         </p>
      <p>
         <input type="submit" value="Submit">
         </p>
      </form>
   <p>
      Age: %s<br>
      Hobbies: %s
      </p>
   </body>
</html>"""


def application(environment, start_response):
	
	try:
		request_body_size = int(environment.get('CONTENT_LENGTH', 0))
	except (ValueError):
		request_body_size = 0

	request_body = environment['wsgi.input'].read(request_body_size)
	d = parse_qs(request_body)
	
	age = d.get('age', [''])[0]
	hobbies = d.get('hobbies', [])
	age = escape(age)
	hobbies = [escape(hobby) for hobby in hobbies]

	
	resp_body = html % (age or 'Empty', ','.join(hobbies or ['No Hobbies']))	
	resp_status = '200 OK'
	resp_headers = [
						('Content-Type', 'text/html'),
						('Content-Length', str(len(resp_body)))
					 ]

	start_response(resp_status, resp_headers)
	return [resp_body]

httpd = make_server('localhost', 8051, application)
httpd.serve_forever()

# server.listen(('0.0.0.0', 8000))
# server.run(application)